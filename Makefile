TARGET = roomba

CC = clang++

INCDIR = include
SRCDIR = src
OBJDIR = obj
BINDIR = .
SOURCES := $(wildcard $(SRCDIR)/*.cpp)
HEADERS := $(wildcard $(INCDIR)/*.h)
OBJECTS := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
BINARIES = main
rm = rm -f

GCCFLAGS = -Wall -Wpedantic -g
CFLAGS = -g 
CLANG_FLAGS = -g -Wall -Wpedantic -g
LLFLAGS = -std=c++17 -pthread
LFLAGS = 

$(BINDIR)/$(TARGET): $(OBJECTS)
	@echo "$(CC) $(LLFLAGS) $(OBJECTS) $(LFLAGS) -o $@"
	@$(CC) $(LLFLAGS) $(OBJECTS) $(LFLAGS) -o $@

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@echo "$(CC) $(LLFLAGS) $(CLANG_FLAGS) -c $< -o -I./$(INCDIR)"
	@$(CC) $(LLFLAGS) $(CLANG_FLAGS) -I./$(INCDIR) -c $< -o $@

.PHONY: clean
clean:
	@$(rm) $(OBJECTS)

.PHONY: remove
remove: clean
	@$(rm) $(BINDIR)/$(TARGET)
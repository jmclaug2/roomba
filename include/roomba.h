#pragma once 

#include <string>
#include <iostream>
#include <cstddef>
#include <iomanip>
#include <vector>
#include <sstream>
#include <thread>
#include <cstdlib>

#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>

class Route {
    std::ostringstream st;
 public:
    static std::string const endl() {
        static std::string rv = "\n";
        return rv;
    }
    template <typename T> 
    Route &operator << (T const &t) {
       st << t;
       return *this;
    }
    ~Route() {
       std::cout << st.str();
    }
    void commit() {
        std::cout << st.str();
        st.clear();
    }
};

class Roomba {
public:
    static int const 
    open_flags = O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK;
    
    /* port is the filepath of the port. */
    std::string const port; 

    /* fd is the file descriptor returned by ::open. */
    int const fd;
    
    Roomba(std::string port):
        port(port), 
        fd(::open(port.c_str(), Roomba::open_flags)),
        TheThread{} {

        Open = (fd < 0)? false: true;
        if (!Open) {
            TheErrno = errno;
        }
        if (is_open()) {
            TheThread = std::thread{&Roomba::read_thread, this};
        }
        if (happy()) {
            setup();
        }
    }
    ~Roomba();
    /* is_open returns true if the serial port was opened. */
    bool is_open();
    /* write returns the number of bytes written. */
    size_t write(std::vector<std::byte> input);

    bool parse(std::vector<std::string> &input);

private:

    std::thread TheThread;

    /* Open maintains the open state of the serial port. */
    bool Open = false;
    /* TheErrno is assigned to the value of errno at certain steps. */
    int TheErrno = 0;
    /* happy returns true if TheErrno is 0. happy writes to cerr when it
       returns false. */
    bool happy();
    /* setup sets the serial port attributes. We make a bunch of 
       assumption based on the profile of the device. Setup returns true
       if the port is setup properly. */
    bool setup();

    void read_thread();
};
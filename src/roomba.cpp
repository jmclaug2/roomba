#include "roomba.h"

Roomba::~Roomba() {
    if (is_open()) {
        ::close(fd);
        Open = false;
        TheThread.join();
    }
}


bool Roomba::
is_open() {
    return Open;
}

size_t Roomba::
write(std::vector<std::byte> input) {

    int wn = ::write(fd, &input[0], input.size() * sizeof(std::byte));

    // TODO: remove print statements
    { Route rout; 
        rout << "wrote " << input.size() << "/" << wn << " bytes: ";
        for (auto b : input)
        rout << std::hex << std::setw(2) << std::setfill('0') 
             << static_cast<int>(b) << " ";
        rout << Route::endl();
    }

    usleep((input.size() + 128) * 100);
    
    return wn; 
}

bool Roomba::
parse(std::vector<std::string> &input) {
    if (input[0] == "reset") {
        write({std::byte(7)});
    }
    else if (input[0] == "move") {
        short left  = std::stoi(input[1]);
        short right = (input.size() > 2)? std::stoi(input[2]): std::stoi(input[1]);

        write({std::byte(145), 
               std::byte((right & 0xFF00) >> 8),
               std::byte(right & 0x00FF),
               std::byte((left & 0xFF00) >> 8),
               std::byte(left & 0x00FF)});
    }
    else if (input[0] == "start") {
        if (input.size() > 1 && input[1] == "safe") {
            write({std::byte(128), std::byte(131)});
        }
        else {
            write({std::byte(128)});
        }  
    }
    else if (input[0] == "safe") {
        write({std::byte(131)});
    }
    else if (input[0] == "data") {
        int id  = std::stoi(input[1]);
        write({std::byte(142), std::byte(id)});
    }
    return true;
}

bool Roomba::
happy() {

    if (TheErrno != 0) {
        Route() << "errno: " << TheErrno << " : " << strerror(TheErrno)
                << Route::endl();
        return false;
    }
    else {
        return true ;
    }
}

bool Roomba::
setup() {
    struct termios tty;
    
    if (tcgetattr (fd, &tty) != 0) TheErrno = errno;
    if (!happy()) return false;

    tcflush(fd,TCIFLUSH);

    // NOTE: WEIRDNESS; the robot switches to 19200 baud for an 
    //       undetermined reason when it's reset sometimes. 

    cfsetospeed(&tty, B115200);
    cfsetispeed(&tty, B115200);

    // cfsetospeed(&tty, B19200);
    // cfsetispeed(&tty, B19200);

    tty.c_cflag     = CS8 | CLOCAL | CREAD;
    tty.c_iflag     = IGNPAR;
    tty.c_oflag     = 0;
    tty.c_lflag     = 0;
    tty.c_cc[VMIN]  = 0;
    tty.c_cc[VTIME] = 0;

    if (tcsetattr (fd, TCSANOW, &tty) != 0) TheErrno = errno;
    if (!happy()) return false;

    return true;
}

void Roomba::
read_thread() {
    int n; 
    char buf [1028];
    while(Open) {

        memset(&buf, 0, sizeof(char) * 1028);
        n = ::read(fd, buf, sizeof buf);
        if (n > 0) {
            { Route rout;
            rout << "{" << n << "}";
            for (int i = 0; i < n; ++i) {
                if (buf[i] < 32 || buf[i] > 126) {
                    rout << "\\" << static_cast<int>(buf[i]);
                }
                else {
                    rout << buf[i];
                }
            }}
        }
        usleep(1000);
    } 
}



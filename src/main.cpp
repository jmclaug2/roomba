#include "roomba.h"

#include <iostream>
#include <cstddef>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>

/*
std::vector<std::byte> handle_input() {
    std::string buffer;
    std::getline(std::cin, buffer);
    std::stringstream ss(buffer); 
    int i; 
    std::vector<std::byte> input;
    while (ss >> i) {
        if (i < 0 || i > 255) {
            Route() << "weird input \"" << i << "\" ignored." 
                    << Route::endl;
        }
        else {
            input.push_back(std::byte(i));
        }
    }
    return input;
}
*/

/* handle_input parse's input of multiple decimal integers from cin. */ 
std::vector<std::string> handle_input() {
    std::string buffer;
    std::getline(std::cin, buffer);
    std::istringstream iss{buffer};

    std::vector<std::string> tokens{
        std::istream_iterator<std::string>{iss},
        std::istream_iterator<std::string>{}};

    return tokens;
}

int main(int argc, char *argv[]) {

    Roomba roomba{"/dev/tty.usbserial-DN026CVN"};
    
    if (roomba.is_open()) {
        Route() << "Roomba is connected, fd:" << roomba.fd 
                << Route::endl();
        
        while (true) {
            auto v = handle_input();
            if (v.size() > 0) {
                if(v[0] == "exit") {
                    break;
                }
                else {
                    if(!roomba.parse(v)) {
                        Route() << "weird input ignored." 
                                << Route::endl();
                    }
                }
            }
        }
    }

    return 0;
}




